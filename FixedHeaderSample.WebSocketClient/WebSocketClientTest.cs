using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FixedHeaderSample.WebSocketClient
{
    public class WebSocketClientTest
    {
        private readonly WebSocket4NetHelper _webSocket4NetHelper;
        private CancellationTokenSource _tokenSource;

        public WebSocketClientTest()
        {
            _webSocket4NetHelper = new WebSocket4NetHelper("ws://localhost:4041");
            _webSocket4NetHelper.Opened += OnWebSocketOpened;
            _webSocket4NetHelper.Closed += OnWebSocketClosed;
            _webSocket4NetHelper.Error += OnWebSocketError;
            _webSocket4NetHelper.MessageReceived += OnMessageReceived;
            _webSocket4NetHelper.DataReceived += OnDataReceived;
        }

        public void Startup()
        {
            _webSocket4NetHelper.Start();

            _tokenSource = new CancellationTokenSource();
            SendDataAsync(_tokenSource.Token).GetAwaiter();
        }

        public void Shutdown()
        {
            _webSocket4NetHelper.Stop();

            _tokenSource?.Cancel(false);
            _tokenSource = null;
        }

        public void SendData(string data)
        {
            _webSocket4NetHelper.Send(data);
        }

        #region WebSocket4Net event handlers

        private void OnWebSocketOpened()
        {
            Console.WriteLine(@"WebSocket connect.");
        }

        private void OnWebSocketClosed(string errorMessage)
        {
            Console.WriteLine($@"WebSocket closed: {errorMessage}.");
        }

        private void OnWebSocketError(Exception exception)
        {
            Console.WriteLine($@"WebSocket error: {exception.Message}.");
        }

        private void OnDataReceived(byte[] data)
        {
            Console.WriteLine($@"OnReceiveBinaryData: {data.Length}.");
        }

        private void OnMessageReceived(string data)
        {
            Console.WriteLine($@"OnMessageReceived: {data}.");
        }

        #endregion

        #region GetRandomData

        private static byte[] GetRandomData(Random random)
        {
            var value1 = random.Next(0, 500) / 10f; //0~50℃
            var value2 = random.Next(30, 60); //30~60%
            var temperature = (short) (value1 * 10); //温度
            var humidity = (short) (value2 * 10); //湿度
            Console.WriteLine($@"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} temperature: {value1}℃, humidity: {value2}%.");

            var dataBuffer = new byte[4];
            dataBuffer[0] = (byte) (temperature >> 8);
            dataBuffer[1] = (byte) (temperature & 0xFF);
            dataBuffer[2] = (byte) (humidity >> 8);
            dataBuffer[3] = (byte) (humidity & 0xFF);

            var header = new byte[] {0xFF, 0xFE};
            var len = (short) (dataBuffer.Length + 5);
            var lenBuffer = GetBuffer(len);

            var cks = GetSumData(header);
            cks += GetSumData(lenBuffer);
            cks += GetSumData(dataBuffer);

            var buffer = new byte[len];

            //Header
            var offset = 0;
            Buffer.BlockCopy(header, 0, buffer, offset, header.Length);
            //Length
            offset += header.Length;
            Buffer.BlockCopy(lenBuffer, 0, buffer, offset, lenBuffer.Length);
            //Data
            offset += lenBuffer.Length;
            Buffer.BlockCopy(dataBuffer, 0, buffer, offset, dataBuffer.Length);
            //CKS
            offset += dataBuffer.Length;
            buffer[offset] = cks;

            return buffer;
        }

        private static byte[] GetBuffer(short value)
        {
            var buffer = new byte[2];
            buffer[0] = (byte) (value << 8 & 0xFF); //高位
            buffer[1] = (byte) (value & 0x00FF); //低位
            return buffer;
        }

        private static byte GetSumData(byte[] data)
        {
            return data.Aggregate<byte, byte>(0, (current, item) => (byte) (current + item));
        }

        #endregion

        private async Task SendDataAsync(CancellationToken token)
        {
            var random = new Random(DateTime.Now.Millisecond);
            while (true)
            {
                var data = GetRandomData(random);
                var text = string.Join("-", data.ToArray().Select(t => t.ToString("X")));
                Console.WriteLine($@"SendData: {DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} {text}.");
                _webSocket4NetHelper.Send(data);

                await Task.Delay(3000, token);
                if (token.IsCancellationRequested)
                {
                    break;
                }
            }

            Console.WriteLine($@"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} SendData exit.");
        }
    }
}
