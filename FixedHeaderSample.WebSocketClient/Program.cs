using System;

namespace FixedHeaderSample.WebSocketClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = new WebSocketClientTest();
            test.Startup();

            Console.WriteLine();
            Console.WriteLine("Press key 'q' to stop it!");

            while (true)
            {
                var line = Console.ReadLine()?.ToUpper();
                if (line == "Q")
                {
                    break;
                }

                test.SendData(line);
            }

            test.Shutdown();

            Console.WriteLine();
            Console.WriteLine("The websocket client was stopped!");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            Console.WriteLine();
        }
    }
}
