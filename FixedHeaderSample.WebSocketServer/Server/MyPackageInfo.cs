using SuperSocket.ProtoBase;

namespace FixedHeaderSample.WebSocketServer.Server
{
    public class MyPackageInfo : IKeyedPackageInfo<short>
    {
        public short Key { get; set; }
        public byte[] Body { get; set; }
    }
}
