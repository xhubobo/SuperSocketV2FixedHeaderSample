using System;
using System.Threading;
using System.Threading.Tasks;
using SuperSocket.WebSocket.Server;

namespace FixedHeaderSample.WebSocketServer.Server
{
    public class MyWebSocketSession : WebSocketSession
    {
        public int MessageSent => _messageSent;
        public int MessageClientReceived => _messageClientReceived;

        private int _messageSent;
        private int _messageClientReceived;

        protected override async ValueTask OnSessionConnectedAsync()
        {
            await SendAsync(SessionID);
        }

        public void Ack()
        {
            Interlocked.Increment(ref _messageClientReceived);
        }

        public override async ValueTask SendAsync(string message)
        {
            await base.SendAsync(message);
            Interlocked.Increment(ref _messageSent);
        }

        public void PrintStats()
        {
            var speed = _messageSent / LastActiveTime.Subtract(StartTime).TotalSeconds;
            Console.WriteLine($"Sent {_messageSent} messages, received {_messageClientReceived} messages, {speed:F1}.");
        }

        public void ReceiveData(float temperature, short humidity)
        {
            Console.WriteLine($@"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} Receive data: " +
                              $@"temperature = {temperature}℃, " +
                              $@"humidity = {humidity}%.");
        }
    }
}
