using System.Buffers;
using SuperSocket.Command;
using SuperSocket.WebSocket;

namespace FixedHeaderSample.WebSocketServer.Server
{
    public class MyPackageConverter : IPackageMapper<WebSocketPackage, MyPackageInfo>
    {
        /// +-------+---+----------------------+
        /// |request| l |                      |
        /// | type  | e |     request body     |
        /// |  (2)  | n |                      |
        /// |       |(2)|                      |
        /// +-------+---+----------------------+

        private const int HeaderSize = 4; //Header总长度

        // ReSharper disable once UnusedMember.Local
        private const int HeaderLenOffset = 2; //长度offset

        public MyPackageInfo Map(WebSocketPackage package)
        {
            var reader = new SequenceReader<byte>(package.Data);
            reader.TryReadBigEndian(out short packageKey);

            var body = package.Data.Slice(HeaderSize).ToArray();
            return new MyPackageInfo
            {
                Key = packageKey,
                Body = body
            };
        }
    }
}
