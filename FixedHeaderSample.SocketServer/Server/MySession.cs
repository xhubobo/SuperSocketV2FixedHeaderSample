﻿using System;
using System.Threading.Tasks;
using SuperSocket.Channel;
using SuperSocket.Server;

namespace FixedHeaderSample.SocketServer.Server
{
    public class MySession : AppSession
    {
        protected override async ValueTask OnSessionConnectedAsync()
        {
            Console.WriteLine($@"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} " +
                              $@"New Session connected: {RemoteEndPoint}.");
            await Task.Delay(0);
        }

        protected override async ValueTask OnSessionClosedAsync(CloseEventArgs e)
        {
            Console.WriteLine($@"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} " +
                              $@"Session {RemoteEndPoint} closed: {e.Reason}.");
            await Task.Delay(0);
        }

        public void ReceiveData(float temperature, short humidity)
        {
            Console.WriteLine($@"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} Receive data: " +
                              $@"temperature = {temperature}℃, " +
                              $@"humidity = {humidity}%.");
        }
    }
}
