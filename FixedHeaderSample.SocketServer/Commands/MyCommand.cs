﻿using System;
using System.Buffers;
using System.Threading.Tasks;
using FixedHeaderSample.SocketServer.Server;
using SuperSocket;
using SuperSocket.Command;

namespace FixedHeaderSample.SocketServer.Commands
{
    /// <summary>
    /// 0xFF 0xFE
    /// </summary>
    [Command(Key = unchecked((short) (0xFF * 256 + 0xFE)))]
    public class MyCommand : IAsyncCommand<MyPackageInfo>
    {
        public async ValueTask ExecuteAsync(IAppSession session, MyPackageInfo package)
        {
            await Task.Delay(0);
            HandleData(session, package.Body);
        }

        private void HandleData(IAppSession session, byte[] body)
        {
            var buffer = new ReadOnlySequence<byte>(body);
            var reader = new SequenceReader<byte>(buffer);
            reader.TryReadBigEndian(out short temperature); //以大端方式读取short类型数据
            reader.TryReadBigEndian(out short humidity); //以大端方式读取short类型数据
            humidity = (short) (humidity / 10);
            (session as MySession)?.ReceiveData(temperature / 10f, humidity);
        }
    }
}
