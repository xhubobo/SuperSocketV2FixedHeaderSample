﻿using System;
using System.Threading.Tasks;
using FixedHeaderSample.SocketServer.Commands;
using FixedHeaderSample.SocketServer.Server;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SuperSocket;
using SuperSocket.Command;

namespace FixedHeaderSample.SocketServer
{
    class Program
    {
        static async Task Main()
        {
            //创建宿主：用Package类型和PipelineFilter类型创建SuperSocket宿主。
            var host = SuperSocketHostBuilder.Create<MyPackageInfo, MyPipelineFilter>()
                //注入Service
                .UseHostedService<MyService<MyPackageInfo>>()
                //注入Session
                .UseSession<MySession>()
                //注入命令
                .UseCommand((commandOptions) =>
                {
                    commandOptions.AddCommand<MyCommand>(); //数据命令
                })
                //配置日志
                .ConfigureLogging((hostCtx, loggingBuilder) =>
                {
                    //控制台打印日志
                    loggingBuilder.AddConsole();
                    loggingBuilder.SetMinimumLevel(LogLevel.Warning);
                })
                .Build();
            try
            {
                await host.RunAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
