﻿using System;

namespace FixedHeaderSample.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var socketSession = new SocketSession();
            socketSession.Start(2021);

            Console.WriteLine();
            Console.WriteLine("Press key 'q' to stop it!");
            while (Console.ReadKey().KeyChar.ToString().ToUpper() != "Q")
            {
                Console.WriteLine();
            }

            socketSession.Stop();

            Console.WriteLine();
            Console.WriteLine("The server was stopped!");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            Console.WriteLine();
        }
    }
}
